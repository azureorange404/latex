# my L<sup>A</sup>&Tau;<sub>&Epsilon;</sub>&Chi; collection

## templates

I have made some templates for articles and school worksheets.

- [article.tex](./article.tex)
- [mathe.tex](./mathe.tex)
- [presentation.tex](./presentation.tex)
- [template.tex](./template.tex)
- [worksheet.tex](./worksheet.tex)

### fish

I then use some fish scripting to autogenerate documents from their respective template and some source if necessary.
These can be seen in my [config.fish](https://gitlab.com/azureorange404/dotfiles/-/blob/main/.config/fish/config.fish)

## packages

I added my default packages as .sty files to simply just load them in the preamble of my .tex documents.

- [preamble.sty](./packages/preamble.sty) (I use this in almost every .tex document)
- [bibliography.sty](./packages/bibliography.sty) (for articles where I need to reference Literature)
- [titlepage.sty](./packages/titlepage.sty) (For an article where my fellow team members wanted a colorful titlepage)
- [presentation.sty](./packages/presentation.sty)
- [worksheetstyle.sty](./packages/worksheetstyle.sty) (For school worksheets; Uses the ["Deutschschweizer Basisschrift"](https://basisschrift.ch) as its main font.)

## biblatex

My .bib file I use to collect all the literature I ever used.

- [lib.bib](./bib/lib.bib)
